import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { BookService } from 'src/app/data.services/book.service';
import { Book } from 'src/app/interfaces/book.model';

@Component({
  selector: 'app-book-update',
  templateUrl: './book-update.component.html',
  styleUrls: ['./book-update.component.css']
})
export class BookUpdateComponent implements OnInit {
  public notesUpdateForm: FormGroup;
  private subs: Subscription[] = [];

  private bookId: string;
  public currentBook: Book;

  constructor(private formBuilder: FormBuilder,
              private bookService: BookService,
              private activeRoute: ActivatedRoute) {
    this.activeRoute.paramMap.subscribe(params => {
      this.bookId = params.get('id');
    });

    const sub = this.bookService.getBookById(this.bookId).subscribe(book => {
      this.currentBook = book;
    });

    this.subs.push(sub);

    this.notesUpdateForm = this.formBuilder.group({
      name: ['', [Validators.required]],
      authors: ['', [Validators.required]],
      description: [''],
      subject: [''],
      image: [''],
      url: [''],
    });
  }

  public get name() {
    return this.notesUpdateForm.get('name');
  }

  public get authors() {
    return this.notesUpdateForm.get('authors');
  }

  public get url() {
    return this.notesUpdateForm.get('url');
  }

  public submitForm() {
    if (!this.notesUpdateForm.get('name').touched) {
      this.notesUpdateForm.get('name').setValue(this.currentBook.name);
    }
    if (!this.notesUpdateForm.get('authors').touched) {
      this.notesUpdateForm.get('authors').setValue(this.currentBook.authors);
    }

    if(!this.notesUpdateForm.valid) {
      window.alert("Форма није исправна!");
      return;
    }

    const dataToUpdate = [];

    for (const field in this.notesUpdateForm.controls) { 
      const control = this.notesUpdateForm.get(field);
      if (control.touched) {
        const obj = {
          name: field,
          value: this.notesUpdateForm.get(field).value
        };
        dataToUpdate.push(obj);
      }
    }

    console.log(dataToUpdate);

    const sub = this.bookService.updateBookById(dataToUpdate, this.bookId).subscribe(res => {
      window.alert(res.message);
    });
    
    this.subs.push(sub);
  }

  ngOnInit() {
  }

  ngOnDestory() {
    this.subs.forEach(sub => sub.unsubscribe());
  }

}
