import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { BookService } from 'src/app/data.services/book.service';
import { CartService } from 'src/app/data.services/cart.service';
import { Book } from 'src/app/interfaces/book.model';

@Component({
  selector: 'app-book-info',
  templateUrl: './book-info.component.html',
  styleUrls: ['./book-info.component.css']
})
export class BookInfoComponent implements OnInit {
  public book: Book;
  public bookId: string;
  private subs: Subscription[] = [];

  constructor(private bookService: BookService,
              private cartService: CartService,
              private route: ActivatedRoute) {
    route.paramMap.subscribe(params => {
      this.bookId = params.get('id');
    }); 
    const sub = this.bookService.getBookById(this.bookId).subscribe(book => {
      this.book = book;
    });
    this.subs.push(sub);
  }

  public addToCart() {
    this.cartService.addToCart(this.book);
  }

  public openLink(url: string) {
    window.open(url, '_blank');
  }

  ngOnInit() {
  }

  ngOnDestroy() {
    this.subs.forEach(sub => sub.unsubscribe());
  }
}
