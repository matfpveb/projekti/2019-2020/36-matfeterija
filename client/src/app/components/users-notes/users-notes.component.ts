import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { BookService } from 'src/app/data.services/book.service';
import { UserService } from 'src/app/data.services/user.service';
import { RouterNavigation } from 'src/app/helper.services/router.navigation';
import { Book } from 'src/app/interfaces/book.model';

@Component({
  selector: 'app-users-notes',
  templateUrl: './users-notes.component.html',
  styleUrls: ['./users-notes.component.css']
})
export class UsersNotesComponent implements OnInit {
  public books: Observable<Book[]>;

  constructor(private bookService: BookService,
              private userService: UserService,
              public routerNavigation: RouterNavigation) { 
    this.books = this.bookService.getBooksByUserId(this.userService.korisnikPodaci._id);
  }

  ngOnInit() {
  }

}
