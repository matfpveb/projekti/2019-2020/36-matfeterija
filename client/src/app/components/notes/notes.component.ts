import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { BookService } from 'src/app/data.services/book.service';

@Component({
  selector: 'app-notes',
  templateUrl: './notes.component.html',
  styleUrls: ['./notes.component.css']
})
export class NotesComponent implements OnInit {
  public notesForm: FormGroup;
  private subs: Subscription[] = [];

  constructor(private formBuilder: FormBuilder,
              private bookService: BookService) { 
    this.notesForm = this.formBuilder.group({
      name: ['', [Validators.required]],
      authors: ['', [Validators.required]],
      description: [''],
      subject: [''],
      image: [''],
      url: ['', [Validators.required]],
    });
  }

  public get name() {
    return this.notesForm.get('name');
  }

  public get authors() {
    return this.notesForm.get('authors');
  }

  public get url() {
    return this.notesForm.get('url');
  }

  public submitForm(data: Object) {
    if(!this.notesForm.valid) {
      window.alert("Форма није исправна!");
      return;
    }

    const sub = this.bookService.addBook(data).subscribe(book => {
      window.alert("Успешно сте додали " + book.name);
    });
    
    this.subs.push(sub);
  }

  ngOnInit() {
  }

  ngOnDestory() {
    this.subs.forEach(sub => sub.unsubscribe());
  }

}
