import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Observable, Subscription } from 'rxjs';
import { CartService } from 'src/app/data.services/cart.service';
import { OrderService } from 'src/app/data.services/order.service';
import { Book } from 'src/app/interfaces/book.model';
import { Order } from 'src/app/interfaces/order.model';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {
  public books: Book[] = [];
  public checkoutForm: FormGroup;
  private newOrderSub: Subscription;

  constructor(private cartService: CartService,
              private formBuilder: FormBuilder,
              private orderService: OrderService) { 
    this.books = this.cartService.getBooks();

    this.checkoutForm = this.formBuilder.group({
      name: ['', [Validators.required]],
      address: ['', [Validators.required, Validators.pattern('[ A-Za-z0-9]+ [0-9]+(/[0-9]+)?')]],
      email: ['', [Validators.required, Validators.email]]
    });
  }

  // BITNO: ovde nam ne treba name.value, vec sam taj objekat, da bismo
  // mogli da iscitavamo da li ima greske.
  public get name() {
    return this.checkoutForm.get('name');
  }

  public get address() {
    return this.checkoutForm.get('address');
  }

  public get email() {
    return this.checkoutForm.get('email');
  }

  public submitForm(data: Object) {
    if(!this.checkoutForm.valid) {
      window.alert("Form not valid!");
      return;
    }

    this.newOrderSub = this.orderService.createAnOrder(data).subscribe(order => {
      window.alert('Поруџбина је успешно креирана!');
      this.cartService.emptyCart();
    });
  }

  ngOnInit() {
  }

  ngOnDestory() {
    this.newOrderSub.unsubscribe();
  }

}
