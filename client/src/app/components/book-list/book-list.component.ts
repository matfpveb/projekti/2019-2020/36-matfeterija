import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { BookService } from 'src/app/data.services/book.service';
import { RouterNavigation } from 'src/app/helper.services/router.navigation';
import { Book } from 'src/app/interfaces/book.model';

@Component({
  selector: 'app-book-list',
  templateUrl: './book-list.component.html',
  styleUrls: ['./book-list.component.css']
})
export class BookListComponent implements OnInit {
  public books: Observable<Book[]>;
  
  constructor(private bookService: BookService,
              public routerNavigation: RouterNavigation) { 
    this.books = this.bookService.getBooks();
  }

  ngOnInit() {
  }

}
