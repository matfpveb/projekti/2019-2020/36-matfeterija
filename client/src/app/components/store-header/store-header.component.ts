import { Component, OnInit, Renderer2, ViewChild, ElementRef, OnDestroy } from '@angular/core';
import { AuthService } from '../../data.services/auth.service';
import { get } from 'scriptjs';

import { Subscription } from 'rxjs';
import { UserService } from '../../data.services/user.service';
import { RouterNavigation } from '../../helper.services/router.navigation';
import { EscRegExp } from '../../helper.services/esc-reg-exp';
import { User } from 'src/app/interfaces/user.model';
import { BookService } from 'src/app/data.services/book.service';
import { Book } from 'src/app/interfaces/book.model';

@Component({
  selector: 'app-store-header',
  templateUrl: './store-header.component.html',
  styleUrls: ['./store-header.component.css']
})

export class StoreHeaderComponent implements OnInit, OnDestroy {

  @ViewChild('options', { static: false })
  private options: ElementRef;

  @ViewChild('search', { static: false })
  private search: ElementRef;
  

  private subs: Subscription[] = [];

  public showMyBookstoreOptions = false;
  public showSearch = false;
  public showEmptySearch = false;

  public allBooks: Book[] = [];
  public foundBooks: Book[] = [];


  constructor(private auth: AuthService,
              private renderer: Renderer2,
              public routerNavigation: RouterNavigation,
              private bookService: BookService) {
    // Provera klikova van menija sa podesavanjima
    this.renderer.listen('window', 'click', (e: Event) => {
      if (!this.options.nativeElement.contains(e.target)) {
          this.showMyBookstoreOptions = false;
      }

      if (!this.search.nativeElement.contains(e.target)) {
        this.showSearch = false;
    }
   });
  }

  ngOnInit() {
    get('https://kit.fontawesome.com/c059048980.js', () => {
        // FontAwesome library has been loaded...
    });
  }

  ngOnDestroy() {
    this.subs.forEach(sub => sub.unsubscribe());
  }

  triggerMyBookstoreOptions() {
    this.showMyBookstoreOptions = !this.showMyBookstoreOptions;
  }

  getBooks() {
    this.showSearch = true;
    this.subs.push(
      this.bookService.getBooks().subscribe(books => {
        this.allBooks = books;
      }, () => {
        //this.userService.sviKorisniciPodaci = [];
      })
    );
  }

  searchBooks(event) {
    if (event.target.value) {
      const reg = new EscRegExp(event.target.value, 'i');
      this.foundBooks = this.allBooks.filter(
        book => book.name.match(reg) || 
                book.authors.match(reg) || 
                (book.subject && book.subject.match(reg))
      );

      if (this.foundBooks.length) {
        this.showSearch = true;
        this.showEmptySearch = false;
      } else {
        this.showEmptySearch = true;
      }

    } else {
      this.showSearch = false;
    }
  }

}

