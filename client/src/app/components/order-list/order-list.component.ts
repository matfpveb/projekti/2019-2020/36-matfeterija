import { Component, OnInit } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { BookService } from 'src/app/data.services/book.service';
import { OrderService } from 'src/app/data.services/order.service';
import { RouterNavigation } from 'src/app/helper.services/router.navigation';
import { Book } from 'src/app/interfaces/book.model';
import { Order } from 'src/app/interfaces/order.model';

@Component({
  selector: 'app-order-list',
  templateUrl: './order-list.component.html',
  styleUrls: ['./order-list.component.css']
})
export class OrderListComponent implements OnInit {
  public userOrders: Order[] = [];
  public subs: Subscription[] = [];
  //public userOrders: Observable<Order[]>;

  constructor(private orderService: OrderService,
              private bookService: BookService,
              public routerNavigation: RouterNavigation) { 
    this.refreshOrders();
  }

  public refreshOrders() {
    const sub = this.orderService.getCurrentUserOrders().subscribe(orders => {
      this.userOrders = orders;
    });

    this.subs.push(sub);
  }

  ngOnInit() {
  }

  ngOnDestory() {
    this.subs.forEach(sub => sub.unsubscribe());
  }

}
