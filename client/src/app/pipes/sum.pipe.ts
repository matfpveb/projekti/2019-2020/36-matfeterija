import { Pipe, PipeTransform } from '@angular/core';
import { Book } from '../interfaces/book.model';

@Pipe({
  name: 'sum'
})
export class SumPipe implements PipeTransform {

  transform(books: Book[], ...args: any[]): any {
    return books.map(book => book.price)
                .reduce((acc, value) => acc + value);
  }

}
