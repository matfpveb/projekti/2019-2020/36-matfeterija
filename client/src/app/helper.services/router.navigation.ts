import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable({ providedIn: 'root' })
export class RouterNavigation {

    constructor(private router: Router) {}

    idiNaProfil(alas: string) {
        this.router.navigate(['/profil', alas]);
    }

    idiNaPocetnuStranu() {
        this.router.navigate(['/']);
    }

    idiNaPodesavanja() {
        this.router.navigate(['/podesavanja']);
    }

    idiNaObjavu(id: string) {
        this.router.navigate(['/objava', id]);
    }

    idiNaProdavnicu() {
        this.router.navigate(['/prodavnica']);
    }

    idiNaKorpu() {
        this.router.navigate(['/prodavnica/korpa']);
    }

    idiNaKnjigu(id: string) {
        this.router.navigate(['/prodavnica/knjiga', id]);
    }

    idiNaIzmeniKnjigu(id: string) {
        this.router.navigate(['/prodavnica/knjiga/izmeni', id]);
    }

    idiNaMojePorudzbine(){
        this.router.navigate(['/prodavnica/moje-porudzbine']);
    }

    idiNaDodajBeleske() {
        this.router.navigate(['/prodavnica/dodaj-beleske']);
    }

    idiNaMojeBeleske(){
        this.router.navigate(['/prodavnica/moje-beleske']);
    }
}
