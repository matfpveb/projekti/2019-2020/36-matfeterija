import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Order } from '../interfaces/order.model';
import { CartService } from './cart.service';
import { UserService } from './user.service';

@Injectable({
  providedIn: 'root'
})
export class OrderService {
  private readonly orderUrl = 'http://localhost:3000/orders/';

  constructor(private http: HttpClient,
              private userService: UserService,
              private cartService: CartService) {}

  public getCurrentUserOrders(): Observable<Order[]> {
    const userId = this.userService.korisnikPodaci._id;
    return this.http.get<Order[]>(this.orderUrl + 'user/' + userId);
  }

  public createAnOrder(formData: any): Observable<Order> {
    if(!this.userService.korisnikPodaci) {
      window.alert('Морате се улоговати да бисте направили поруџбину!');
      return;
    }
    const userId = this.userService.korisnikPodaci._id;
    

    const body = {
      customerId: userId,
      name: formData.name,
      books: this.cartService.getBooks(),
      address: formData.address,
      email: formData.email
    }

    return this.http.post<Order>(this.orderUrl, body);
  }

}
