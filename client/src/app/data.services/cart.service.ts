import { Injectable } from '@angular/core';
import { Book } from '../interfaces/book.model';

@Injectable({
  providedIn: 'root'
})
export class CartService {
  public books: Book[] = [];

  constructor() { }

  public addToCart(book: Book) {
    this.books.push(book);
  }

  public getBooks() {
    return this.books;
  }

  public emptyCart() {
    this.books = [];
  }
}
