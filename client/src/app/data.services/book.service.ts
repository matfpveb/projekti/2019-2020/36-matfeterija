import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Book } from '../interfaces/book.model';

@Injectable({
  providedIn: 'root'
})
export class BookService {
  public books: Observable<Book[]>;
  private readonly booksUrl = 'http://localhost:3000/books/';

  constructor(private http: HttpClient) { 
    this.books = this.refreshBooks();
  }

  public refreshBooks() {
    // Postoji dva nacina da se ovo uradi. Jedan je da se pretplatimo
    // na tok koji nam vracaju pozivi funkcija ka serveru, a drugi je 
    // da koristimo pipe async. S obzirom da necemo raditi nikakve druge
    // operacije nad knjigama, a da bismo skratili ceo proces pretplacivanja
    // i odjavljivanja, iskoristicemo pristup sa pipe-om async.
    return this.http.get<Book[]>(this.booksUrl);
  }

  public getBooks(): Observable<Book[]> {
    return this.books;
  }

  public getBookById(id: string): Observable<Book> {
    return this.http.get<Book>(this.booksUrl + id);
  }

  public getBooksByUserId(id: string): Observable<Book[]> {
    return this.http.get<Book[]>(this.booksUrl + 'user/' + id);
  }

  public addBook(data): Observable<Book> {
    const body = {
      name: data.name,
      price: data.price || 0,
      authors: data.authors || null,
      description: data.description || null,
      subject: data.subject || null,
      image: data.image || null,
      url: data.url || null,
    }
    return this.http.post<Book>(this.booksUrl, body);
  }

  public updateBookById(data, id): Observable<any> {
    return this.http.patch<Book>(this.booksUrl + id, data);
  }

  // TODO(teodora): Dodati apstraktnu klasu za obradu greske!
}
