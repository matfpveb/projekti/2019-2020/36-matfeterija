import { Book } from "./book.model";

export interface Order {
    _id: string,
    customerId: string,
    name: string,
    books: Book[],
    address: string,
    email: string
}