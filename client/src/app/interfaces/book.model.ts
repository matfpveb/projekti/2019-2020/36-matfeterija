export interface Book {
    _id: string;
    name: string;
    price: number;
    authors?: string;
    description?: string;
    subject?: string;
    image?: string;
    url?: string;
}