import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IndexComponent } from './components/index/index.component';
import { GuestComponent } from './components/guest/guest.component';
import { SettingsComponent } from './components/settings/settings.component';
import { AuthGuard } from './helper.services/auth.guard';
import { ProfileComponent } from './components/profile/profile.component';
import { PostPageComponent } from './components/post-page/post-page.component';
import { StoreComponent } from './components/store/store.component';
import { CartComponent } from './components/cart/cart.component';
import { BookInfoComponent } from './components/book-info/book-info.component';
import { OrderListComponent } from './components/order-list/order-list.component';
import { NotesComponent } from './components/notes/notes.component';
import { UsersNotesComponent } from './components/users-notes/users-notes.component';
import { BookUpdateComponent } from './components/book-update/book-update.component';


const routes: Routes = [
  { path: '', component: IndexComponent },
  { path: 'podesavanja', component: SettingsComponent, canActivate: [AuthGuard]},
  { path: 'profil/:alas', component: ProfileComponent, canActivate: [AuthGuard]},
  { path: 'objava/:id', component: PostPageComponent, canActivate: [AuthGuard]},
  { path: 'gosti', component: GuestComponent },
  { path: 'prodavnica', component: StoreComponent},
  { path: 'prodavnica/korpa', component: CartComponent},
  { path: 'prodavnica/knjiga/:id', component: BookInfoComponent},
  { path: 'prodavnica/knjiga/izmeni/:id', component: BookUpdateComponent},
  { path: 'prodavnica/moje-porudzbine', component: OrderListComponent},
  { path: 'prodavnica/dodaj-beleske', component: NotesComponent},
  { path: 'prodavnica/moje-beleske', component: UsersNotesComponent},
  { path: '**', redirectTo: '' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
