const mongoose = require('mongoose');

const bookSchema = new mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    userId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'users'
    },
    name: {
        type: String,
        required: true
    },
    price: {
        type: Number,
        required: true
    },
    authors: String,
    description: String,
    subject: String,
    image: String,
    url: String
});

const bookModel = mongoose.model('books', bookSchema);

module.exports = bookModel;