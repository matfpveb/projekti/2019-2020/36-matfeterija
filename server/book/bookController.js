const mongoose = require('mongoose');
const Book = require("./bookModel");

module.exports.getBooks = async function (req, res, next) {
    try {
        const books = await Book.find({}).exec();
        res.status(200).json(books);
    } catch(err) {
        next(err);
    }
}

module.exports.getBookById = async function (req, res, next) {
    const id = req.params.id;
    
    try {
        const book = await Book.findById(id).exec();
        // Ako knjiga nije nadjena, treba vratiti 404 error
        if(!book) {
            res.status(404).json({message: 'Knjiga nije pronadjena!'});
        }
        res.status(200).json(book);
    } catch(err) {
        next(err);
    }
}

module.exports.getBooksByUserId = async function (req, res, next) {
    const userId = req.params.id;

    try {
        const books = await Book.find({ userId: userId }).exec();
        res.status(200).json(books);
    } catch(err) {
        next(err);
    }
}

module.exports.addBook = async function (req, res, next) {
    // Kreirati objekat koji sadrzi sve potrebne podatke
    const bookObject = {
        _id: new mongoose.Types.ObjectId,
        userId: req.body.userId,
        name: req.body.name,
        price: req.body.price,
        authors: req.body.authors,
        description: req.body.description,
        subject: req.body.subject,
        image: req.body.image,
        url: req.body.url
    };

    // Kreirati mongoose objekat od bookObject
    const newBook = new Book(bookObject);

    try {
        const savedBook = await newBook.save();
        res.status(200).json(savedBook);
    } catch(err) {
        next(err);
    }
}

module.exports.updateBookById = async function (req, res, next) {
    const bookId = req.params.id;

    try {
        const book = await Book.findById(bookId).exec();

        for (let i = 0; i < req.body.length; i++) {
            const option = req.body[i];
            book[option.name] = option.value;
        }

        const updatedBook = await book.save();
        res.status(200).json({message: 'Knjiga je uspesno azurirana.'});
    } catch(err) {
        next(err);
    }
}

module.exports.deleteBookById = async function (req, res, next) {
    const id = req.params.id;

    try {
        await Book.deleteOne({_id: id}).exec();

        res.status(200).json({message: 'Knjiga uspesno obrisana!'});
    } catch(err) {
        next(err);
    }
}