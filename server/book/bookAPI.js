const express = require('express');
const controller = require('./bookController');

const router = express.Router();

router.get('', controller.getBooks);
router.post('', controller.addBook);

router.get('/:id', controller.getBookById);
router.patch('/:id', controller.updateBookById);
router.delete('/:id', controller.deleteBookById);

router.get('/user/:id', controller.getBooksByUserId);

module.exports = router;