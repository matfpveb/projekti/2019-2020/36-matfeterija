# Matfeterija 👋
![Version](https://img.shields.io/badge/version-1.0.0-blue.svg?cacheSeconds=2592000)
[![License: ISC](https://img.shields.io/badge/License-ISC-yellow.svg)](#)

> Server za Matf message board

## Podešavanje

```sh
npm install
npm install nodemon -g
npm install run-rs -g
```

## Pokretanje

Windows:
```sh
nodemon server.js
```

Ubuntu:
```sh
nodemon server.js
```



***
_This README was generated with ❤️ by [readme-md-generator](https://github.com/kefranabg/readme-md-generator)_
