const mongoose = require('mongoose');
const Order = require('./orderModel');

module.exports.getOrders = async function (req, res, next) {
    try {
        const orders = await Order.find({}).populate('books').exec();
        res.status(200).json(orders);
    } catch(err) {
        next(err);
    }
};

module.exports.addOrder = async function (req, res, next) {
    // Create order object.
    const orderObj = {
        _id: new mongoose.Types.ObjectId,
        customerId: req.body.customerId,
        name: req.body.name,
        books: req.body.books.map(book => book._id),
        address: req.body.address,
        email: req.body.email
    };

    console.log(orderObj);

    const newOrder = new Order(orderObj);

    try {
        const addedOrder = await newOrder.save();
        res.status(200).json(addedOrder);
    } catch(err) {
        next(err);
    }
}

module.exports.getOrderById = async function (req, res, next) {
    const id = req.params.id;

    try {
        const order = await (await Order.findById(id)).populate('books').exec();

        // If an order with the provided id is not found, we should
        // return a 404 error
        if(!order) {
            res.status(404).json({message: 'Order not found.'});
        }
        res.status(200).json(order);
    } catch(err) {
        next(err);
    }
}

module.exports.getOrdersByUserId = async function (req, res, next) {
    const userId = req.params.id;

    try {
        const userOrders = await Order.find({customerId: userId}).populate('books').exec();
        res.status(200).json(userOrders);
    } catch(err) {
        next(err);
    }
}

module.exports.deleteOrderById = async function (req, res, next) {
    const id = req.params.id;

    try {
        await Order.deleteOne({_id: id}).exec();
        res.status(200).json({message: 'Order successfully deleted.'});
    } catch(err) {
        next(err);
    }
}