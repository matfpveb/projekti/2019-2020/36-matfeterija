const mongoose = require('mongoose');

const orderSchema = new mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    customerId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'users'
    },
    name: {
        type: String,
        required: true
    },
    books: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'books'
    }],
    address: {
        type: String,
        required: true,
    },
    email: {
        type: String,
        required: true
    }
});

const orderModel = mongoose.model('orders', orderSchema);

module.exports = orderModel;