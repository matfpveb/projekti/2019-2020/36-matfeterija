const express = require('express');
const controller = require('./orderController');

const router = express.Router();

router.get('/', controller.getOrders);
router.post('/', controller.addOrder);

router.get('/:id', controller.getOrderById);
router.delete('/:id', controller.deleteOrderById);

router.get('/user/:id', controller.getOrdersByUserId);

module.exports = router;