'use strict';

const express = require('express');
const monk = require('monk');

// Poruke su zasebna ruta
const router = express.Router();

const db = monk('mongodb://localhost:27017/matfeterija');
const poruke = db.get('poruke');

router.get('/', (req, res) => {
  poruke
    .find()
    .then(poruke => {
      res.json(poruke);
    });
});

function isValidPoruka(poruka) {
  return poruka.name && poruka.name.toString().trim() !== '' &&
      poruka.content && poruka.content.toString().trim() !== '';
}

router.post('/', (req, res) => {
  if(isValidPoruka(req.body)) {
    const poruka = {
      name: req.body.name.toString().trim(),
      content: req.body.content.toString().trim(),
      created: new Date()
    };

    poruke.insert(poruka).then(createdPoruka => {
          res.json(createdPoruka);
        }).catch(err => console.error(err));

  }
  else {
    res.status(422);
    res.json({
      message: 'Morate uneti ime i poruku!'
    });
  }
});

module.exports = router;
